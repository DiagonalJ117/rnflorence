
import React from 'react';

import { Text, View, TouchableOpacity, StyleSheet, ListView, StatusBar, Modal, Alert, FlatList, Picker} from 'react-native';
import { Card, Divider, Header, Input, Button } from 'react-native-elements';
import { ScrollView  } from 'react-native-gesture-handler';
import {  FAB, Dialog, Portal, Provider, TextInput } from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/FontAwesome';


const Realm = require('realm');

const EntrySchema = {
  name: 'Entry',
  primaryKey: 'id',
  properties: {
    id: 'string',
    amount: 'double',
    description:'string',
    dateTime: 'date',
    type: 'string'
  }
};

const entryTypes = {
  expense: 'expense',
  income: 'income'
}


let realm = new Realm({schema: [EntrySchema]});



function Item({ amount, description, dateTime, type}) {
  return (
    <View style={{ alignSelf: "stretch", flexDirection:"column", justifyContent: "center"}}>
     
      <Card containerStyle={[styles.cardStyle, type == 'income' ? styles.incomeCardStyle : styles.cardStyle ]}>
        <Text>${amount}</Text>
        
        <Text>{description}</Text>
        <Text style={{textAlign:"right"}}>{dateTime}</Text>
      </Card>
      
    </View>
  );
}


 
export default class Home extends React.Component {
  constructor(props){
    super(props);
    // var expenses = realm.objects('Entry');
    this.incomes = [];
    this.state = {
      initialBudget: 0.00,
      currentBudget: 0.00,
      sumExpenses: 0.00,
      editBudgetDialogV: false,
      addExpenseDialogV: false,
      addIncomeDialogV:false,
      editEntryDialogV: false,
      entryInput:'',
      entryDescInput:'',
      entryTypeInput:'',
      expense:{id: 0,amount:0, description:'', dateTime: new Date()},

      expensesHolder: [],
      incomesHolder: [],
      expenses: [],
      selectedEntryId:'',
      randId:0,
      openFab: false
      
    }

  }

  findExpsense (id) {
    console.log("si entra lol" + id)
    var expense = realm.objects('Entry').filtered('id ='+ "'"+id +"'");
    console.log(expense[0]);
  }
  
  GenerateRandomNumber = () => {

    var RandomNumber = Math.floor(Math.random() * 1000000) + 1;

    return RandomNumber.toString();
  }
  
  //Entry Dialog SHOW/HIDE Methods
  _showAddExpDialog = () => {
    this.setState({entryInput: ''});
    this.setState({entryDescInput: ''});
    this.setState({ addExpenseDialogV: true });
  }
  _hideAddExpDialog = () => this.setState({ addExpenseDialogV: false });

  _showEditEntryDialog = (id) => {
    var entryQ = realm.objects('Entry').filtered('id ='+ "'"+id +"'");
    var entry = entryQ[0];
    this.setState({selectedEntryId:String(entry.id)})
    this.setState({editEntryDialogV:true});
    this.setState({entryInput: String(entry.amount)});
    this.setState({entryDescInput: entry.description});
    this.setState({entryTypeInput: entry.type});
  }
  _hideEditEntryDialog = () => this.setState({editEntryDialogV:false});

  //Income Dialog SHOW/HIDE Methods
  _showAddIncDialog = () => {
    this.setState({entryInput: ''});
    this.setState({entryDescInput: ''})
    this.setState({addIncomeDialogV: true});
  }
  _hideAddIncDialog = () => this.setState({addIncomeDialogV:false});

  _showEditBudgetDialog = () => this.setState({editBudgetDialogV: true});
  _hideEditBudgetDialog = () => this.setState({editBudgetDialogV:false});


  //CRUD Entry
  _addExpense = () => {
    
      realm.write(() =>{
        realm.create('Entry',{
          id: this.GenerateRandomNumber(),
          amount: Number(this.state.entryInput),
          description: this.state.entryDescInput,
          dateTime: new Date(),
          type:'expense'
        });
      });
    this._hideAddExpDialog();
    console.log(realm.objects('Entry').filtered("type ="+"'expense'"));
  }


  _editEntry = () => {
    realm.write(() => {
      
      var expId = this.state.selectedEntryId;
      console.log("Id", expId);
      var expenseObj = realm.objects('Entry').filtered('id ='+ "'"+this.state.selectedEntryId +"'");
      console.log(expenseObj);
      if(expenseObj.length > 0){
        this._hideEditEntryDialog();
        expenseObj[0].amount = Number(this.state.entryInput);
        expenseObj[0].description = this.state.entryDescInput;
        expenseObj[0].type = this.state.entryTypeInput;
        Alert.alert(
          'Exito!',
          'Gasto editado correctamente!'
        );
      }else{
        alert("ERROR:Entry not found.");
      }
      

    })

  }

  _deleteEntry = () => {
    realm.write(() => {
      
      var expenseObj = realm.objects('Entry').filtered('id ='+ "'"+this.state.selectedEntryId +"'");
      if( expenseObj.length > 0){
        this._hideEditEntryDialog();
        realm.delete(realm.objects('Entry').filtered('id ='+ "'"+this.state.selectedEntryId +"'"));
        Alert.alert("Exito.", "Gasto Borrado Correctamente");
        
      }else{
        this._hideEditEntryDialog();
        alert("Error al borrar gasto.")
      };
    })
    console.log("delete expense" + this.state.selectedEntryId);
  }

  _addIncome = () => {
    realm.write(() =>{
      realm.create('Entry',{
        id: this.GenerateRandomNumber(),
        amount: Number(this.state.entryInput),
        description: this.state.entryDescInput,
        dateTime: new Date(),
        type: 'income'
      });
    });
  this._hideAddIncDialog();
  console.log(realm.objects('Entry').filtered("type ="+"'income'"));
  }

  componentDidMount(){
    this.setState({expensesHolder: realm.objects('Entry')});
  }

  
  
  headerHistorial = () =>{
    return(
      <View style={styles.dividerStyle}>
      <Text style={{textAlign: "center"}}>Historial</Text>
    </View>
    );
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={styles.dividerStyle}
      />
    );
  }
  
  render() {
    
    return (
    <Provider>
      <View style={styles.MainContainer}>
        
        {/* GASTOS */}
        {/* Agregar Gasto*/}
        <Portal>
                  <Dialog
            visible={this.state.addExpenseDialogV}
            animationType={'slide'}
            onDismiss={this._hideAddExpDialog}
            style={{}}
            
            
          >

            <Dialog.Title>Agregar Gasto</Dialog.Title>
            <Dialog.Content>
            <TextInput label="Cantidad" keyboardType="numeric" 
            onChangeText={(value) => this.setState({ entryInput: value})}
            value={this.state.entryInput}
            
            ></TextInput>
            <TextInput label="Descripcion"
            onChangeText={(value) => this.setState({entryDescInput: value})}
            value={this.state.entryDescInput}
            ></TextInput>
            </Dialog.Content>
            <Dialog.Actions style={{alignContent:"flex-start"}}>
              <Button title="Agregar" onPress={this._addExpense}></Button>
            </Dialog.Actions>
          </Dialog>

                    {/* INGRESOS */}
          {/* Agregar Ingreso */}
          <Dialog
            visible={this.state.addIncomeDialogV}
            animationType={'slide'}
            onDismiss={this._hideAddIncDialog}
            style={{}}
            >

            <Dialog.Title>Agregar Ingreso</Dialog.Title>
            <Dialog.Content>
            <TextInput label="Cantidad" keyboardType="numeric" 
            onChangeText={(value) => this.setState({ entryInput: value})}
            value={this.state.entryInput}
            
            ></TextInput>
            <TextInput label="Descripcion"
            onChangeText={(value) => this.setState({entryDescInput: value})}
            value={this.state.entryDescInput}
            ></TextInput>
            </Dialog.Content>
            <Dialog.Actions style={{alignContent:"flex-start"}}>
              <Button title="Agregar" onPress={this._addIncome}></Button>
            </Dialog.Actions>
          </Dialog>

          {/* Editar Entrada (gasto o ingreso)*/}
          <Dialog
            visible={this.state.editEntryDialogV}
            animationType={'slide'}
            onDismiss={this._hideEditEntryDialog}
            style={{}}
          >

            <Dialog.Title>Editar Entrada</Dialog.Title>
            <Dialog.Content>
            <TextInput label="Cantidad" keyboardType="numeric" 
            onChangeText={(value) => this.setState({ entryInput: value})}
            value={this.state.entryInput}
            
            ></TextInput>
            <TextInput label="Descripcion"
            onChangeText={(value) => this.setState({entryDescInput: value})}
            value={this.state.entryDescInput}
            ></TextInput>
            <Picker
            selectedValue={this.state.entryTypeInput}
            onValueChange={(itemValue, itemIndex) => this.setState({entryTypeInput: itemValue})}
            >
              <Picker.Item label="Ingreso" value="income"/>
              <Picker.Item label="Gasto" value="expense"/>


            </Picker>
            </Dialog.Content>
            <Dialog.Actions style={{justifyContent:"space-between"}}>
              <Button onPress={this._deleteEntry} buttonStyle={{ backgroundColor: "red"}} icon={<Icon name="trash" size={20} color="white"></Icon>}></Button>
              <Button  title="Editar" onPress={this._editEntry}></Button>
              
            </Dialog.Actions>
          </Dialog>
          



          {/* BUDGET */}
          {/* Editar presupuesto */}
          <Dialog
            visible={this.state.editBudgetDialogV}
            animationType={'slide'}
            onDismiss={this._hideEditBudgetDialog}
            style={{}}
            >

            <Dialog.Title>Editar Presupuesto Inicial</Dialog.Title>
            <Dialog.Content>
            <TextInput label="Cantidad" keyboardType="numeric" 
            onChangeText={(value) => this.setState({ initialBudget: value})}
            value={this.state.initialBudget}
            
            ></TextInput>
            </Dialog.Content>
            <Dialog.Actions style={{alignContent:"flex-start"}}>
              <Button title="Actualizar" onPress={this._hideEditBudgetDialog}></Button>
            </Dialog.Actions>
          </Dialog>
        
          </Portal>
              
        <View>
          <Header placement="center" centerComponent={{ text: "Florence" , style:{ color: "#FFFFFF", fontSize: 25} }}></Header>

        </View>
        <View style={styles.budgetsContainer}>
          
          <View style={styles.budgetTopRow}>
            <TouchableOpacity onPress={() => this._showEditBudgetDialog()}>
            <Card title="Presupuesto Inicial" >
            <Text style={{textAlign:"center"}}>${this.state.initialBudget}</Text>
            </Card>
            </TouchableOpacity>
            <Card title="Suma de Gastos">
            <Text style={{textAlign:"center"}}>${realm.objects('Entry').filtered("type ="+"'expense'").sum('amount')}</Text>
            </Card>
          </View>
          <View style={{alignSelf:"stretch" }}>
            <Card title="Presupuesto Disponible">
              <View styles={{flex:1}}><Text style={{textAlign:"center"}}>${ (Number(this.state.initialBudget) + realm.objects('Entry').filtered("type ="+"'income'").sum('amount') - realm.objects('Entry').filtered("type ="+"'expense'").sum('amount'))}</Text></View>
            </Card>
          </View>
          

        </View>
        
        <View  style={{ flex: 1.5,justifyContent: 'center', alignItems: "center", flexDirection:"column" }}>

        
          <FlatList
          style={{alignSelf:"stretch"}}
          ListHeaderComponent={this.headerHistorial}
          data={realm.objects('Entry')}
          renderItem={({item, id}) => 
          <TouchableOpacity onPress={() => this._showEditEntryDialog(item.id)}>
          <Item id={item.id} type={item.type} amount={item.amount} description={item.description} dateTime={item.dateTime.toUTCString()}> </Item>
          </TouchableOpacity>
        
        }
          
          extraData = {this.state.expenses}
          
          ></FlatList>
          
          
        </View>
        
      </View>
      <Portal>
      <FAB.Group
      
            fabStyle= {styles.fab}
             open={this.state.open}
             icon={this.state.open ? 'close' : 'plus'}
             actions={[
               { icon: 'plus', onPress: () => console.log('Pressed add') },
               
               { icon: 'minus', style: {backgroundColor: "red"}, label: 'Gasto', onPress: () => this._showAddExpDialog() },
               { icon: 'plus', style: {backgroundColor: "green"}, label: 'Ingreso', onPress: () => this._showAddIncDialog() },
             ]}
             onStateChange={({ open }) => this.setState({ open })}
             onPress={() => {
               if (this.state.open) {
                 // do something if the speed dial is open
               }
             }}
           />
         
      </Portal>
      </Provider>
    );
  }
}
const styles = StyleSheet.create({
  fab: {

    backgroundColor: "green"
  },
  cardStyle: {
    flex: 1, 
    flexDirection:"column"
  },
  incomeCardStyle:{
    backgroundColor:"#A5D6A7"
  },
  expenseCardStyle:{
    backgroundColor:"white"
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
  dividerStyle: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    alignSelf: "stretch"
  },
  MainContainer: {
    flex: 1
  },
  budgetsContainer: {
    flex: 1,
    justifyContent: 'space-between', 
    alignItems: "center", 
    flexDirection:"column", 
    marginBottom: 0
  },
  budgetTopRow: {
    flex: 1,
    justifyContent: "space-around", 
    alignItems: 'center', 
    flexDirection:"row"
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    alignSelf: "stretch"
  },
  title: {
    fontSize: 15,
  },

});