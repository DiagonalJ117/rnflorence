import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity} from 'react-native';
import {  FAB, Dialog, Portal, Provider, TextInput } from 'react-native-paper';


class EditExpenseDialog extends Component{
    state = {
      visible: false,
    }

    GenerateRandomNumber = () => {

      var RandomNumber = Math.floor(Math.random() * 1000000) + 1;
    
      return RandomNumber.toString();
    }

    _showDialog = () => this.setState({ visible: true });

    _hideDialog = () => this.setState({ visible: false });

    _editExpense = () => {

    //   this.expenses.push({ id: this.props.id,amount: this.props.amount, description: this.props.description, dateTime: );
      
      this._hideDialog();
      console.log(this.expenses);
    }

    render = () => {
      const { visible } = this.props;
        return (
          visible &&
            <Portal>
            <Dialog
      visible={this.state.visible}
      animationType={'slide'}
      onDismiss={() => {this._hideDialog}}
      style={{}}
      >

      <Dialog.Title>Editar Gasto</Dialog.Title>
      <Dialog.Content>
      <TextInput label="Cantidad" keyboardType="numeric" 
      onChangeText={(value) => {this.props.amount}}
      value={this.props.amount}
      
      ></TextInput>
      <TextInput label="Descripcion"
      onChangeText={(value) => this.props.description}
      value={this.props.description}
      ></TextInput>
      </Dialog.Content>
      <Dialog.Actions style={{alignContent:"flex-start"}}>
        <Button title="Agregar" onPress={this._editExpense}></Button>
      </Dialog.Actions>
    </Dialog>

    </Portal>
        )
    }
}

export default AddExpenseDialog;